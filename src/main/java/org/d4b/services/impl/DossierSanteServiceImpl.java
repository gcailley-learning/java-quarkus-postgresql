package org.d4b.services.impl;

import io.smallrye.mutiny.Uni;
import org.d4b.models.DossierSante;
import org.d4b.repository.DossierSanteRepository;
import org.d4b.services.DossierSanteService;


import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
@Transactional(Transactional.TxType.MANDATORY)
public class DossierSanteServiceImpl implements DossierSanteService {
    @Inject
    DossierSanteRepository dossierSanteRepository;

    @Override
    public Uni<List<DossierSante>> fetchAll(Optional<String> start, Optional<String> limit) {
        System.out.println(dossierSanteRepository.count().await().toString());
        Uni<List<DossierSante>> ds = dossierSanteRepository.listAll();
        return ds;
    }

    @Override
    public Uni<Void> persist(DossierSante newDossierSante) {
       return dossierSanteRepository.persist(newDossierSante);
    }
}
