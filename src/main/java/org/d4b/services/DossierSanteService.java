package org.d4b.services;

import io.smallrye.mutiny.Uni;
import org.d4b.models.DossierSante;

import java.util.List;
import java.util.Optional;

public interface DossierSanteService {

    public Uni<List<DossierSante>> fetchAll(Optional<String> start, Optional<String> limit);

    /**
     * Save the "Dossier Sante" Object.
     * @param newDossierSante DossierSante to save
     */
   public Uni<Void> persist(DossierSante newDossierSante);
}
