package org.d4b.models;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of = {"description", "assure"})
@Entity
@Cacheable
public class DossierSante extends PanacheEntityBase {
    @Id
    @GeneratedValue
    String id;
    @Column(length = 40)
    String description;

    @OneToOne
    Assure assure;
}
