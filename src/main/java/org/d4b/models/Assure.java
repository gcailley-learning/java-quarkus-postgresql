package org.d4b.models;

import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of = {"id", "name"})
@Entity
@Cacheable
public class Assure extends PanacheEntityBase {
    @Id
    @GeneratedValue
    String id;
    String name;
    String phone;
    String email;
}
