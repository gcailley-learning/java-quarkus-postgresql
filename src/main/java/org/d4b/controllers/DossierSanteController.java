package org.d4b.controllers;


import io.smallrye.mutiny.Uni;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.d4b.builder.AssureBuilder;
import org.d4b.builder.DossierSanteBuilder;
import org.d4b.dto.DossierSanteDTO;
import org.d4b.models.Assure;
import org.d4b.models.DossierSante;
import org.d4b.services.DossierSanteService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.modelmapper.ModelMapper;

@ApplicationScoped
@Slf4j(topic = "DossierSante")
public class DossierSanteController {

    @Inject
    DossierSanteService dossierSanteService;
    ModelMapper modelMapper = new ModelMapper();

    public Uni<List<DossierSanteDTO>> fetchDossiersSante(Optional<String> start, Optional<String> limit) {

        // convert to DTO
        return dossierSanteService.fetchAll(start, limit)
                .onItem().transform(listOfDossierSantes -> {
            List<DossierSanteDTO> data = listOfDossierSantes.stream()
                    .map(ds -> modelMapper.map(ds, DossierSanteDTO.class))
                    .collect(Collectors.toList());
            return data;
        });
    }

    public Uni<DossierSante> persistDossierSante(JsonObject bodyAsJson) {
        Assure newAssure = AssureBuilder.create("name", "phone", "email");
        DossierSante newDossierSante = DossierSanteBuilder.create("description envoyée", newAssure);

        // TODO convert to DTO
        return dossierSanteService.persist(newDossierSante).replaceWith(Uni.createFrom().item(newDossierSante));
    }
}
