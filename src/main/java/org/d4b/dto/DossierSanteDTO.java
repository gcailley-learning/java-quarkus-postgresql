package org.d4b.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;


@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of = {"description", "assure"})
public class DossierSanteDTO {

        String id;
        String description;
        AssureDTO assure;
}
