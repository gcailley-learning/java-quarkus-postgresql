package org.d4b.builder;

import org.d4b.models.Assure;
import org.d4b.models.DossierSante;

public class AssureBuilder {
    public static Assure create(
            String name,
            String phone,
            String email ) {
        return new Assure(null, name, phone, email);
    }
}
