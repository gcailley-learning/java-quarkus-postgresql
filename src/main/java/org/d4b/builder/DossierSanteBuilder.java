package org.d4b.builder;

import org.d4b.models.Assure;
import org.d4b.models.DossierSante;

public class DossierSanteBuilder {
    public static DossierSante create(String description, Assure assure) {
        return new DossierSante(null, description, assure);
    }
}
