package org.d4b.repository;

import io.quarkus.hibernate.reactive.panache.PanacheRepository;
import org.d4b.models.DossierSante;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class DossierSanteRepository implements PanacheRepository<DossierSante> {

}
