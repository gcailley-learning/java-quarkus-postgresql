package org.d4b.routes;


import io.quarkus.vertx.web.Route;
import io.quarkus.vertx.web.RoutingExchange;
import io.smallrye.mutiny.Uni;
import io.vertx.core.http.HttpMethod;
import org.d4b.controllers.DossierSanteController;
import org.d4b.dto.DossierSanteDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class DossierSanteRoutes {
    @Inject
    private DossierSanteController dossierSanteController;

    @Route(path = "/dossiers-sante", methods = HttpMethod.GET, produces = "application/json")
    Uni<List<DossierSanteDTO>> fetchMeetups(RoutingExchange ex) {
        Optional<String> limit = ex.getParam("limit");
        Optional<String> start = ex.getParam("start");
        return dossierSanteController.fetchDossiersSante(start, limit);
    }

    @Route(path = "/dossiers-sante", methods = HttpMethod.POST, produces = "application/json")
    Uni<Response> postDossierSante(RoutingExchange ex) {
        return dossierSanteController.persistDossierSante(ex.context().getBodyAsJson())
                .flatMap(dossierSante -> Uni.createFrom()
                        .item(Response.ok(dossierSante).status(Response.Status.CREATED)::build)
        );
    }
}
