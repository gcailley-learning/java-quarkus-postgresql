package org.d4b.routes;

import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;

@QuarkusTest
public class HealthCheckResourceTest {

    @Test
    public void testReadyPoint() {
        given()
          .when().get("/q/health/ready")
          .then()
             .statusCode(200)
                .body("status", equalTo("UP"))
                .body("checks[0].name", equalTo("Reactive PostgreSQL connections health check"))
                .body("checks[0].status", equalTo("UP"))
                .body("checks[1].name", equalTo("My Database connection health check"))
                .body("checks[1].status", equalTo("UP"));
    }
}