package org.d4b.controllers;

import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.mutiny.Uni;
import org.d4b.builder.AssureBuilder;
import org.d4b.builder.DossierSanteBuilder;
import org.d4b.models.Assure;
import org.d4b.models.DossierSante;
import org.d4b.repository.DossierSanteRepository;
import org.d4b.services.DossierSanteService;
import org.d4b.services.impl.DossierSanteServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.inject.Inject;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@QuarkusTest
class DossierSanteControllerTest {
@Inject
    DossierSanteController dossierSanteController;


    Assure mockAssure = AssureBuilder.create("test", "phone test", "test@mockito.com");
    Stream<DossierSante> mockDossierSante = Stream.of(
            DossierSanteBuilder.create("description mock 1", mockAssure),
            DossierSanteBuilder.create("description mock 2", mockAssure)
    );


    @BeforeEach
    void setUp() {
        DossierSanteService mockDossierSanteService = Mockito.mock(DossierSanteServiceImpl.class);
        Mockito.when(mockDossierSanteService.fetchAll(null, null))
                .thenReturn(
                        Uni.createFrom().item(mockDossierSante.collect(Collectors.toList())));

        // record the current mock in the injection context.
        QuarkusMock.installMockForType(mockDossierSanteService, DossierSanteService.class);
    }


    @Test
    public void fetchDossiersSante() {
        // Mocked classes always return a default value
        dossierSanteController.fetchDossiersSante(null, null).subscribe().with(dossiersSante -> {
            dossiersSante.stream().forEach( ds -> {
                Assertions.assertNotNull(ds.getDescription());
                Assertions.assertEquals(mockAssure, ds.getAssure());
            });
        });
    }

    @Test
    void persistDossierSante() {
    }
}